---
title: "Underdog"
date: 2018-01-13T20:44:30-08:00
image: cover/underdogs.jpg
draft: false
hardback: false
authors: "Markus-Zusak"
names: ["Underdogs, Markus Zusak"]
publishers: ["Knopf Books"]
---

Boys are like dogs - ready to bite, bark and beg to be given a chance to show their value.. "I vowed that if I ever got a girl I would treat her right and never be bad or dirty to her or hurt her, ever." Cameron Wolfe is a dirty boy. He knows it. His brother Rube knows it, because he's one too. they could change - but what would it take?
