---
title: "The Book Thief"
date: 2018-01-13T17:46:19-08:00
image: cover/the-book-thief-2.jpg
draft: false
tags: ["book"]
authors: "Markus-Zusak"
names: ["The Book Thief, Markus Zusak"]
publishers: ["Knopf Books"]
---

It’s just a small story really, about among other things: a girl, some words, an accordionist, some fanatical Germans, a Jewish fist-fighter, and quite a lot of thievery ...

Set during World War II in Germany, Markus Zusak’s groundbreaking new novel is the story of Liesel Meminger, a foster girl living outside of Munich. Liesel scratches out a meager existence for herself by stealing when she encounters something she can’t resist – books. With the help of her accordion-playing foster father, she learns to read and shares her stolen books with her neighbors during bombing raids as well as with the Jewish man hidden in her basement before he is marched to Dachau. This is an unforgettable story about the ability of books to feed the soul.
